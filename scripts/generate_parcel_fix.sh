#!/usr/bin/env bash
function get_parcel_packages {
  grep -oh -E "@parcel/[a-z-]+" yarn.lock | sort -u
}

function print_fix {
  echo \""$1@$2"\":
  echo "  peerDependencies:"
  echo "    "\""@parcel/core"\"": $2"
}

pkg_version=$1

for pkg in $(get_parcel_packages); do
  print_fix $pkg $pkg_version
done
