# Mulberry UI

## For nerds

### Scripts

The following scripts should be run in the project root directory.

| Name                                    | Description                                 |
| --------------------------------------- | ------------------------------------------- |
| `yarn run build:ts`                     | Builds and checks the typescript files.     |
| `yarn run clean:ts`                     | Cleans up typescript builds.                |
| `yarn workspace @mulberrypost/ui start` | Starts the parcel server on the UI package. |
| `yarn workspace @mulberrypost/ui build` | Builds the UI package.                      |

### About yarn setup

You'll need to [install yarn](https://yarnpkg.com/getting-started/install).

- We are using yarn 3 Zero-Install features, including Plug-n-play.
- There is a CI job which checks the validity of the yarn cache on merge requests

### About typescript setup

We use `tsc` to build and check the validity of types.

For browser based packages, we use `parcel` to build everything.

`parcel` always looks at the `main:` of a `package.json` which can cause issues when importing an internal package like `@mulberrypost/lib`.
For this reason, we append the `src/` directory to these imports. Example:

```typescript
// This has issues :(
import doThings from '@mulberrypost/lib';

// This works :)
import doThings from '@mulberrypost/lib/src';
```
